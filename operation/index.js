module.exports = {
    get env () {
        return require('./env/');
    },

    get error () {
        return require('./error/');
    },

    get location () {
        return require('./location/');
    }
};
